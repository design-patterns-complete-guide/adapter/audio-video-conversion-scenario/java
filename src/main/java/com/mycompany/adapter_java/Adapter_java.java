package com.mycompany.adapter_java;

//Target Interface
interface MediaPlayer{
    void play();
}
//Adaptee Classes

class MP3Player{
    void playMP3(){
        System.out.println("Playing MP3 Audio");
    }
}
class MP4Player{
    void playMP4(){
        System.out.println("Playing MP4 Video");
    }
}

//Adapter Classes
class MP3Adapter implements MediaPlayer{
    private MP3Player mp3Player;

     public MP3Adapter(MP3Player mp3Player) {
        this.mp3Player = mp3Player;
    }
     public void play(){
         this.mp3Player.playMP3();
     }
    
}
class MP4Adapter implements MediaPlayer{
    private MP4Player mp4Player;

    public MP4Adapter(MP4Player mp4Player) {
        this.mp4Player = mp4Player;
    }
    public void play(){
        this.mp4Player.playMP4();
    }
    
}


public class Adapter_java {

    public static void main(String[] args) {
        MP4Player mp4Player = new MP4Player();
        MP4Adapter mp4Adapter = new MP4Adapter(mp4Player);
        mp4Adapter.play();
        
        MP3Player mp3Player= new MP3Player();
        MP3Adapter mp3Adapter= new MP3Adapter(mp3Player);
        mp3Adapter.play();
        
    
    }
}
